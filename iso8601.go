package iso8601

import (
	"errors"
	"fmt"
	"time"
)

type Time struct {
	time.Time
}

func (t *Time) String() string {
	return String(t.Time)
}

func String(t time.Time) string {
	return t.UTC().Format("2006-01-02T15:04:05-0700")
}

var InvalidTimeErr = errors.New("ISO8601: Invalid Date")

func ParseTime(s string) (t time.Time, err error) {
	var year, month, day, hours, minutes, seconds int
	var cursor, n int
	var tz_sign, tz_hh, tz_mm int
	// parse Points in time, must be one of
	// YYYY-MM-DD
	// YYYYMMDD
	// YYYYMMDDTHHMM
	// YYYY-MM-DDTHH:MM
	// YYYYMMDDTHHMMSS
	// YYYY-MM-DDTHH:MM:SS

	year, month, day, cursor, err = scanDate(s)
	if err != nil {
		return t, err
	}

	t = time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
	if cursor >= len(s) {
		return t, nil
	}

	if s[cursor] == 'T' {
		cursor++
		hours, minutes, n, err = scanHHMM(s[cursor:])
		if err != nil {
			return t, err
		}
		cursor += n
		t = t.Add(time.Duration(hours)*time.Hour + time.Duration(minutes)*time.Minute)

		if cursor >= len(s) {
			return t, nil
		}

		if s[cursor] == ':' {
			cursor++
			for i := 0; i < 2; i++ {
				if cursor >= len(s) || !isDigit(s[cursor]) {
					err = InvalidTimeErr
					return
				}
				seconds = seconds*10 + int(s[cursor]-'0')
				cursor++
			}

			t = t.Add(time.Duration(seconds) * time.Second)
		}

		// Timezone
		if cursor >= len(s) || s[cursor] == 'Z' {
			return t, nil
		}
		if s[cursor] == '-' {
			tz_sign = -1
		} else if s[cursor] == '+' {
			tz_sign = 1
		} else {
			return t, InvalidTimeErr
		}
		cursor++
		var offset time.Duration
		tz_hh, tz_mm, n, err = scanHHMM(s[cursor:])
		if err != nil {
			return
		}
		offset = time.Duration(tz_sign) * (time.Duration(tz_hh)*time.Hour + time.Duration(tz_mm)*time.Minute)
		t = t.Add(offset)
	}
	return t, nil
}

func scanDate(s string) (year, month, day, nBytes int, err error) {
	var extendedForm bool

	if len(s) < 7 {
		err = InvalidTimeErr
		return
	}

	var cursor int
	// parse year
	if s[0] == '-' {
		if s[1] == '-' {
			// Date without year, --12-05
			extendedForm = true
			cursor++
		} else {
			err = InvalidTimeErr
			return
		}
	} else {
		for i := 0; i < 4; i++ {
			if !isDigit(s[cursor]) {
				err = InvalidTimeErr
				return
			}
			year = year*10 + int(s[cursor]-'0')
			cursor++
		}
	}

	if s[cursor] == '-' {
		extendedForm = true
		cursor++
	}
	for i := 0; i < 2; i++ {
		if !isDigit(s[cursor]) {
			err = InvalidTimeErr
			return
		}
		month = month*10 + int(s[cursor]-'0')
		cursor++
	}
	if extendedForm {
		if s[cursor] == '-' {
			cursor++
		} else {
			err = InvalidTimeErr
			return
		}
	}

	for i := 0; i < 2; i++ {
		if !isDigit(s[cursor]) {
			err = InvalidTimeErr
			return
		}
		day = day*10 + int(s[cursor]-'0')
		cursor++
	}

	nBytes = cursor
	return
}

func scanHHMM(s string) (hours, minutes, nBytes int, err error) {
	var cursor int
	if len(s) < 4 {
		err = InvalidTimeErr
		return
	}

	for i := 0; i < 2; i++ {
		if !isDigit(s[cursor]) {
			err = InvalidTimeErr
			return
		}
		hours = hours*10 + int(s[cursor]-'0')
		cursor++
	}

	if s[cursor] == ':' {
		cursor++
	}
	if cursor >= len(s) {
		err = InvalidTimeErr
		return
	}

	for i := 0; i < 2; i++ {
		if !isDigit(s[cursor]) {
			err = InvalidTimeErr
			return
		}
		minutes = minutes*10 + int(s[cursor]-'0')
		cursor++
	}

	nBytes = cursor
	return
}

func isDigit(b byte) bool {
	return b >= '0' && b <= '9'
}

// C-like sscanf
func sscanf(s string, format string, a ...interface{}) int {
	n, err := fmt.Sscanf(s, format, a...)
	if err != nil {
		return -1
	}
	return n
}

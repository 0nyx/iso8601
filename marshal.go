package iso8601

import "errors"

func (t *Time) UnmarshalJSON(b []byte) (err error) {
	if len(b) < 2 {
		err = errors.New("Too short")
		return
	}
	if b[0] == '"' {
		b = b[1:]
	}
	b = b[:len(b)-1]
	t.Time, err = ParseTime(string(b))
	return
}

func (t *Time) MarshalJSON() ([]byte, error) {
  return []byte(t.String()), nil
}

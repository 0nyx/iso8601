package iso8601

import (
	"testing"
	"time"
)

type testCase struct {
	year, month, day, hour, minute, second int
	str                                    string
	valid                                  bool
}

func (tc *testCase) eqToDate(year, month, day int) bool {
	return tc.year == year && tc.day == day && tc.month == month
}

func (tc *testCase) eqToTime(hour, minute, second int) bool {
	return tc.hour == hour && tc.minute == minute
}

func (tc *testCase) eq(t time.Time) bool {
	return tc.eqToDate(t.Year(), int(t.Month()), t.Day()) && tc.eqToTime(t.Hour(), t.Minute(), t.Second())
}

func Test_scanDate(t *testing.T) {
	testCases := []testCase{
		{str: "2018-01-02", year: 2018, month: 1, day: 2, valid: true},
		{str: "20180102", year: 2018, month: 1, day: 2, valid: true},
		{str: "--01-02", year: 0, month: 1, day: 2, valid: true},
		{str: "kldsjfk", valid: false},
		{str: "0102", valid: false},
		{str: "2014-4433", valid: false},
		{str: "-23-33T00:00:00", valid: false},
	}

	for _, test := range testCases {
		year, month, day, _, err := scanDate(test.str)
		if err != nil {
			if test.valid {
				t.Errorf("scanDate triggered false positive error detection, Test: {%+v}, Err: %s", test, err)
			}
			continue
		}
		if !test.valid {
			t.Errorf("scanDate expected an error but did not got one. Test: {%+v}", test)
			continue
		}

		if !test.eqToDate(year, month, day) {
			t.Errorf("scanDate failed to parse date Test: {%+v}, Got: {year: %d, month: %d, day: %d}", test, year, month, day)
		}
	}
}

func Test_scanHHMM(t *testing.T) {
	testCases := []testCase{
		{str: "22:22", hour: 22, minute: 22, valid: true},
		{str: "2222", hour: 22, minute: 22, valid: true},
		{str: "202001", hour: 20, minute: 20, valid: true},
		{str: "", valid: false},
		{str: "dsfdsfsdfdsf", valid: false},
	}

	for _, test := range testCases {
		hour, minute, _, err := scanHHMM(test.str)
		if err != nil {
			if test.valid {
				t.Errorf("scanHHMM triggered false positive error detection, Test: {%+v}, Err: %s", test, err)
			}
			continue
		}
		if !test.valid {
			t.Errorf("scanHHMM expected an error but did not got one. Test: {%+v}", test)
			continue
		}

		if !test.eqToTime(hour, minute, 0) {
			t.Errorf("scanHHMM failed to parse date Test: {%+v}, Got: {hour: %d, minute: %d}", test, hour, minute)
		}
	}
}

func Test_ParseTime(t *testing.T) {
	testCases := []testCase{
		{str: "2018-01-01T22:22", year: 2018, month: 01, day: 01, hour: 22, minute: 22, valid: true},
		{str: "20180101T2222", year: 2018, month: 01, day: 01, hour: 22, minute: 22, valid: true},
		{str: "20180101T2222Z", year: 2018, month: 01, day: 01, hour: 22, minute: 22, valid: true},
		{str: "2018-01-01T22:22Z", year: 2018, month: 01, day: 01, hour: 22, minute: 22, valid: true},
		{str: "2018-01-01T22:22-01:00", year: 2018, month: 01, day: 01, hour: 21, minute: 22, valid: true},
		{str: "2018-01-01T22:22:12-01:00", year: 2018, month: 01, day: 01, hour: 21, minute: 22, second: 12, valid: true},
		{str: "--01-01", year: 0, month: 01, day: 01, valid: true},
		{str: "", valid: false},
		{str: "2018-01-02", year: 2018, month: 1, day: 2, valid: true},
		{str: "20180102", year: 2018, month: 1, day: 2, valid: true},
	}

	for _, test := range testCases {
		dateTime, err := ParseTime(test.str)
		if err != nil {
			if test.valid {
				t.Errorf("ParseTime triggered false positive error detection, Test: {%+v}, Err: %s", test, err)
			}
			continue
		}
		if !test.valid {
			t.Errorf("ParseTime expected an error but did not got one. Test: {%+v}", test)
			continue
		}

		if !test.eq(dateTime) {
			t.Errorf("ParseTime failed to parse date Test: {%+v}, Got: %s", test, dateTime.String())
		}
	}
}

func Test_MarshalJSON(t *testing.T) {

}

func Test_UnmarshalJSON(t *testing.T) {
}
